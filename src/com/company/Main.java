package com.company;

import com.company.model.Books;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Books[] books = new Books[4];
        Books bookTolstoy = new Books();
        Books bookDostoevskiy = new Books();
        Books bookDoyl = new Books();
        //generate
        for (int i = 0; i < 4; i++) {
            bookTolstoy.author = "Толстой";
            bookTolstoy.name = "Анна Каренина ";
            bookTolstoy.year = 1873;
            books[0] = bookTolstoy;

            bookDostoevskiy.name = "Бесы ";
            bookDostoevskiy.author = "Достоевский";
            bookDostoevskiy.year = 1872;
            books[1] = bookDostoevskiy;

            bookDoyl.name = "Долина ужаса ";
            bookDoyl.author = "Дойл";
            bookDoyl.year = 1915;
            books[2] = bookDoyl;
        }

        if (bookDostoevskiy.year < bookDoyl.year && bookDostoevskiy.year < bookTolstoy.year) {
            System.out.println("Автор самой старой книги : " + bookDostoevskiy.author);
        } else if (bookDoyl.year < bookDostoevskiy.year && bookDoyl.year < bookTolstoy.year) {
            System.out.println("Автор самой старой книги : " + bookDoyl.author);
        } else if (bookTolstoy.year < bookDostoevskiy.year && bookTolstoy.year < bookDoyl.year) {
            System.out.println("Автор самой старой книги : " + bookTolstoy.author);
        }


        {
            Scanner scan = new Scanner(System.in);
            System.out.println("Введите автора: ");
            String author = scan.nextLine();
            if (author.equals(bookDostoevskiy.author)) {
                System.out.println("Название книги: " + bookDostoevskiy.name);
            } else if (author.equals(bookDoyl.author)) {
                System.out.println("Название книги: " + bookDoyl.name);
            } else if (author.equals(bookTolstoy.author)) {
                System.out.println("Название книги: " + bookTolstoy.name);
            } else {
                System.out.println("Такой книги нет");
            }
            Scanner scan1 = new Scanner(System.in);
            System.out.println("Введите год");
            int year = Integer.parseInt(scan.nextLine());
            for (int i = 0; i < 3; i++) {
                if (books[i].year < year) {
                    System.out.println("Название: " +books[i].name);
                    System.out.println("Автор: " + books[i].author);
                    System.out.println("Год издания: " + books[i].year);

                }
            }
        }
    }
}




